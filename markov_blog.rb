require 'natto'
require 'pp'
require 'enumerator'

$h = {}
# ----------------形態素解析
# ----------------辞書的なものの作成
def parse_text(text)
	mecab = Natto::MeCab.new
	text = text.strip
	# 形態素解析したデータを配列に分けて突っ込む
	# 先頭にBEGIN、最後にENDを追加
	data = ["BEGIN","BEGIN"]
	mecab.parse(text) do |a|
		if a.surface != nil
			data << a.surface
		end
	end
	data << "END"
	# p data
	data.each_cons(3).each do |a|
		suffix = a.pop
		prefix = a
		$h[prefix] ||= []
		$h[prefix] << suffix
	end
end

# ----------------マルコフ連鎖
def markov()
	p "markov"
	# ランダムインスタンスの生成
	random = Random.new
	# スタートは begin,beginから
	prefix = ["BEGIN","BEGIN"]
	ret = ""
	loop{
		n = $h[prefix].length
		prefix = [prefix[1] , $h[prefix][random.rand(0..n-1)]]
		ret += prefix[0] if prefix[0] != "BEGIN"
		if $h[prefix].last == "END"
			ret += prefix[1]
			break
		end
	}
	p ret
	return ret
end


#test
def parse_article(name)
	str = ""
	begin
	  File.open(name,"r") do |file|
	    file.each_line do |line|
	      str += line.to_s
	    end
			parse_text(str)
	  end
	end
end


#記事を読み込んで辞書作成
parse_article('article1.txt')
parse_article('article2.txt')
parse_article('article3.txt')
#parse_article('article4.txt')


#自動生成
markov()
