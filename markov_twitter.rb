require 'natto'
require 'twitter'
require 'pp'
require 'enumerator'

#Twitter認証情報
CONSUMER_KEY = '5lqNog0Y9zoNVdqLbNORPIwqB'
CONSUMER_SECRET = 'jGLUzO7yG9nsT25D2ButyJJnKKoRal7Vv15dbHP9rMbLbDU8Ql'
ACCESS_TOKEN = '99107959-MOoNFW1GlqimnbJsFIIJlzipOxui3sFsLCth7LgMJ'
ACCESS_TOKEN_SECRET = '31yai3JFnFbylDb6fLLftCOjL3Uol8DX77WiKFTYGQlwG'

# ----------------形態素解析
# ----------------辞書的なものの作成
def parse_text(text)
	mecab = Natto::MeCab.new
	text = text.strip
	# 形態素解析したデータを配列に分けて突っ込む
	# 先頭にBEGIN、最後にENDを追加
	data = ["BEGIN","BEGIN"]
	mecab.parse(text) do |a|
		if a.surface != nil
			data << a.surface
		end
	end
	data << "END"
	# p data
	data.each_cons(3).each do |a|
		suffix = a.pop
		prefix = a
		$h[prefix] ||= []
		$h[prefix] << suffix
	end
end

# ----------------マルコフ連鎖
def markov()
	p "markov"
	# ランダムインスタンスの生成
	random = Random.new
	# スタートは begin,beginから
	prefix = ["BEGIN","BEGIN"]
	ret = ""
	loop{
		n = $h[prefix].length
		prefix = [prefix[1] , $h[prefix][random.rand(0..n-1)]]
		ret += prefix[0] if prefix[0] != "BEGIN"
		if $h[prefix].last == "END"
			ret += prefix[1]
			break
		end
	}
	p ret
	return ret
end

# ----------------Twitter認証
client = Twitter::REST::Client.new do |config|
	config.consumer_key = CONSUMER_KEY
	config.consumer_secret = CONSUMER_SECRET
	config.access_token = ACCESS_TOKEN
	config.access_token_secret = ACCESS_TOKEN_SECRET
end

# ----------------ツイートの読み込み、テーブル作成
# テーブル用ハッシュ
$h = {}
def collect_with_max_id(collection=[], max_id=nil, &block)
	p "collect_with_max_id"
	response = yield(max_id)
	collection += response
	response.empty? ? collection.flatten : collect_with_max_id(collection,response.last.id-1,&block)
end

def client.get_all_tweets(user)
	p "client.get_all_tweets"
	begin
		collect_with_max_id do |max_id|
			options = {count: 100, include_rts: false, exclude_replies: false}
			options[:max_id] = max_id unless max_id.nil?
			user_timeline(user,options)
		end
	rescue => e
		p e.message
		exit
	end
end

for tweet in client.get_all_tweets("asazuke007")
	t = tweet.text
	next if t[0,1] == "@" || t.include?("http")
	puts t
	parse_text(t)
end
markov()
